 [![pipeline status](https://gitlab.com/eloybg97/evolutionaryoptimization/badges/main/pipeline.svg)](https://gitlab.com/eloybg97/evolutionaryoptimization/-/commits/main) 
 
# Evolutionary Optimization

Easy-to-use julia package for function optimization by evolutionary algorithms

## Installation
```
$ ] add EvolutionaryOptimization
```

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
For any issues contact to [elbega@protonmail.com](mailto:elbega@protonmail.com?subject=[Issue])

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## License
Evolutionary Optimization suite is licensed under the MIT license.

## Project status
The project is currently under development
