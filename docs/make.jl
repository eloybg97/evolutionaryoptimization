using Documenter
using EvolutionaryOptimization

makedocs(
    sitename = "Evolutionary Optimization",
    doctest = true,
    pages = [
        "Introduction" => "index.md",
        "Genetic Algorithm" => [
            "Generational Algorithm" => "ga/generational.md",
            "Selection Operator" => "ga/selection_operator.md",
            "Crossover Operator" => "ga/crossover_operator.md",
            "Mutation Operator" => "ga/mutation_operator.md"],
        "Callback" => "callback.md"

    ],
    modules = [EvolutionaryOptimization])