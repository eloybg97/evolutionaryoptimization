# Callbacks
Callbacks are designed to provide information on the evolution of the population over iterations.

## Print Callback
This callback will simply print by standard output the best solution of each generation.

```@docs
    print_callback
```