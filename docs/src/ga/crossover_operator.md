# Crossover Operators
The crossover operators are intended to increase the algorithm's ability to explote the search space. It is very important to have a catalogue of these operators that allows you to adapt to the problem regardless of the type of coding you handle.

### Parameters
* **`parent1`**: First parent chromosome.
* **`parent2`**: Second parent chromosome.

### Output
In the output there are 2 elements:
* **`1st child chromosome`**.
* **`2nd clild chromosome`**.

## K-Point Crossover
This operator consists of dividing the parents into k+1 parts. These parts will be exchanged between the both after each cut-off point.

```@docs
    k_point_crossover(
        parent1::AbstractArray{<:Real, 1},
        parent2::AbstractArray{<:Real, 1};
        k::Int = 1
    )
```


### Keyword
* **`k`**: Number of points you want to cut through.

### Usage
```@example
    using EvolutionaryOptimization
    
    p1 = rand(5)
    p2 = rand(5)

    h1, h2 = k_point_crossover(p1, p2, k = 2)
```
