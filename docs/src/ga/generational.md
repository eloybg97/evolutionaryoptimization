# Generational Algorithm
Genetic algorithms are optimization algorithms based on Charles Darwin's evolutionary theory. The generational genetic algorithm, in particular, creates a complete new population from the previous generation.

```@docs
    GGA
```

## Parameters
* **`crossover`**: Crossover operator that will be used.
* **`selection`**: Selection operator that will be used.
* **`mutation`**: Mutation operator that will be used.
* **`prob_cross`**: The probability of the crossover operation being made.
* **`prob_mut`**: The probability of the mutation operation being made.

```@example
    using EvolutionaryOptimization

    dim = (60, 5)
    pop = rand(dim[1], dim[2])

    f(x) = x[1]^2 - x[2]^2

    crossover(parent1, parent2) = k_point_crossover(parent1, parent2; k = 1) 
    selection(fit) = tournament_selection(fit; k = 3)
    mutation(child) = swap_mutation(child)
    pc = 0.7
    pm = 0.1

    gga = GGA(crossover, selection, mutation, pc, pm)
    res = optimize(gga, f, rand(dim[1], dim[2]) , true, 1000)
```