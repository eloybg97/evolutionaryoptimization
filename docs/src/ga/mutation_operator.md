# Mutation Operator
The mutation operator allows the algorithm to explore the search space. It is very important to have a catalogue of these operators that allows it to adapt to the problem regardless of the type of encoding it handles.

### Parameters
* **`child`**: Child to be mutated.

### Output
The output  2 elements:
* **`mutated_child`**.

## Swap Mutation
This operator consists of exchanging the position of two genomes.
```@docs
   swap_mutation(
        child::AbstractArray{<:Real, 1}
    )
```

### Usage
```@example
    using EvolutionaryOptimization
    
    c = rand(5)
    m = swap_mutation(c)
```
