# Selection Operator
Selection is one of the stages of a genetic algorithm. The objective of this phase is to select the best individuals for the subsequent generation of new individuals. Selection operators are responsible for this phase. 

### Parameter
* **`fit`**: All population fit values 

### Output
The output have one element:
* **`selected_parent`**: The index of the selected individual.

## Tournament Selection
Tournament selection consists of forming a tournament between several individuals and choosing the winner. The winner is chosen on the basis of a criterion based on fitness value.

```@docs
    tournament_selection(
        fit::AbstractArray{<:AbstractFloat, 1};
        k::Int = 3
    )
```

### Keyword
* **`k`**: Number of individuals participating in the tournament 


### Usage
```@example
    using EvolutionaryOptimization

    pop = rand(60, 2)

    f(x) = x[1]^2 + x[2]^2
    fit = map(f, eachrow(pop))

    p = tournament_selection(fit)   
```
