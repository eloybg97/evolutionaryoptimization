# Introduction
EvolutionaryOptimization was born with the idea of developing a package that allows its users to solve optimization problems easily.

Right now, it's in development so it only implements genetic algorithms, but the idea is to implement a lot of evolutionary algorithms so that you can select the one that suits you best based on the type of problem you're trying to solve.

## Optimize Function
This feature will allow you to optimize any problem in the way you want. You just have follow the nexts steps:
* To choose the algorithm of the catalog that gives you this package and configure it as indicated in its respective documentation.
* Select the objective function that describe your problem.
* Initialize the population by yourself. 
* Decide if you want to minimize or minimize.
* Configure the number of times the algorithm will run.
* If you need, you can implement (or choose one already implemented) that will execute after each iterations (See Callbacks)   



```@docs
    optimize(
        alg::Algorithm,
        fobjetive::Function,
        pop::AbstractArray{<:Real, 2},
        maximize::Bool,
        n_iteration::Int = 1,
        callback::Function = nothing_callback
    )
```

### Parameters

* **`alg`**: ¿Wich algorithm do you want to use? This param let you to choose it.
* **`fobjetive`**: The funtion you want to optimize.
* **`pop`**: The population wich you want to start.
* **`maximize`**: Set it to *True* if you want to maximize the function or *False* in other case.
* **`callback`**: That function will be called after each algorithm's iteration. You can use callbacks we give you or develop your own callback function.
* **`n_iteration`**: Number of iteration algorithm will do before stop.

### Output
The output of is an array with next values:
* **`solution`**: point in the space function where optimum fit value is reached.
* **`best_fit`**: optimum fit value.
* **`mean`**: mean of the last iteration fit values.
* **`std`**: standard desviation of the last iteration fit values.

### Usage

```@example
    using EvolutionaryOptimization
    
    function rosenbrock(x::AbstractArray{<:Real, 1})
        return (1 - x[1]) + 100 * (x[2] - x[1]^2)^2
    end

    pop = rand(60, 2)
    
    crossover(parent1, parent2) = k_point_crossover(parent1, parent2; k = 1) 
    selection(fit) = tournament_selection(fit; k = 3)
    mutation(child) = swap_mutation(child) 

    gga = GGA(crossover, selection, mutation, 0.7, 0.1)
    res = optimize(gga, rosenbrock, pop , true)

```

```@docs
    Algorithm
```