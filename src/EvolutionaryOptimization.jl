module EvolutionaryOptimization

"""
    abstract type Algorithm end
"""
abstract type Algorithm end

import Statistics: mean, std
import Random: rand
import StatsBase: sample

export Algorithm
export optimize

export GGA
export tournament_selection
export k_point_crossover
export swap_mutation

export print_callback

include("optimize.jl")
include("ga/generational.jl")
include("ga/crossover_operator.jl")
include("ga/selection_operator.jl")
include("ga/mutation_operator.jl")
include("callback.jl")
end # module
