"""
Print results throw standard output.
"""
function print_callback(
    i::Int64,
    population::AbstractArray{<:Real, 2},
    fit::AbstractArray{<:Real, 1},
    best_idx::Int64)

    best_pop = population[best_idx, :]
    best_fit = fit[best_idx]

    print("---Nº Iteration: $i-----\n")
    print("f($best_pop) = $best_fit\n")
end

function nothing_callback(
    i::Int64,
    population::AbstractArray{<:Real, 2},
    fit::AbstractArray{<:Real, 1},
    best_idx::Int64)

    return nothing
end