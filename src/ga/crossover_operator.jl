"""
k_point_crossover(
        parent1::AbstractArray{<:Real, 1},
        parent2::AbstractArray{<:Real, 1};
        k::Int = 1
    )
"""
function k_point_crossover(
    parent1::AbstractArray{<:Real, 1},
    parent2::AbstractArray{<:Real, 1};
    k::Int = 1
    )
    N = length(parent1)
    
    child1 = copy(parent1)
    child2 = copy(parent2)

    cut = sample(2:N, k, replace = false)
    cut = sort(cut)

    for i = 1:2:k-1
        aux = child1[cut[i]:cut[i+1]-1]
        child1[cut[i]:cut[i+1]-1] = child2[cut[i]:cut[i+1]-1]
        child2[cut[i]:cut[i+1]-1] = aux
    end

    if(k % 2 == 1)
        aux = child1[cut[k]:end]
        child1[cut[k]:end] = child2[cut[k]:end]
        child2[cut[k]:end] = aux
    end

    return reshape(child1, 1, N), reshape(child2, 1, N)
end