"""
    struct GGA <: Algorithm
        cross_op::Function
        select_op::Function
        mutation_op::Function

        prob_cross::AbstractFloat
        prob_mut::AbstractFloat
    end
"""
struct GGA <: Algorithm
    cross_op::Function
    select_op::Function
    mutation_op::Function

    prob_cross::AbstractFloat
    prob_mut::AbstractFloat
end

function one_step(
    gga::GGA,
    pop::AbstractArray{<:Real, 2},
    fit::AbstractArray{<:AbstractFloat, 1},
    fobjetive::Function
    )

    dim = size(pop)
    newpop = typeof(pop)(undef, 0, dim[2])

    n_cross = convert(Int, round(dim[1] * gga.prob_cross, digits = 0)) / 2
    n_mut = convert(Int, round(dim[1] * gga.prob_mut, digits = 0))

    for i = 1:n_cross
        p1_idx = gga.select_op(fit)
        p2_idx = gga.select_op(fit)

        p1 = pop[p1_idx, :]
        p2 = pop[p2_idx, :]

        h1, h2 = gga.cross_op(p1, p2)

        newpop = [newpop; h1 ; h2]
    end

    for i = n_cross+1:dim[1]/2
        p1_idx = gga.select_op(fit)
        p2_idx = gga.select_op(fit)

        h1 = reshape(pop[p1_idx, :], 1, dim[2])
        h2 = reshape(pop[p2_idx, :], 1, dim[2])

        newpop = [newpop; h1 ; h2]
    end

    mut_idx = rand(1:dim[1], n_mut)
    for i = 1:n_mut
        hmut = gga.mutation_op(newpop[i, :])
         newpop[i, :] = hmut
    end

    newfit = map(fobjetive, eachrow(newpop))

    elitism_idx = argmax(fit)
    worst_idx = argmin(newfit)

    newpop[worst_idx, :] = pop[elitism_idx, :]
    newfit[worst_idx] = fit[elitism_idx]

    return newpop, newfit

end