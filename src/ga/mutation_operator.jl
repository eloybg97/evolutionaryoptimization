"""
swap_mutation(
        child::AbstractArray{<:Real, 1}
    )
"""
function swap_mutation(
    child::AbstractArray{<:Real, 1}
    )
    mut_child = copy(child)
    idx = rand(1:length(child), 2)

    aux = mut_child[idx[1]]
    mut_child[idx[1]] = mut_child[idx[2]]
    mut_child[idx[2]] = aux

    return reshape(mut_child, 1, length(mut_child))
end