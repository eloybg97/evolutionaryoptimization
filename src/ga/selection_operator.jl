"""
    tournament_selection(
        fit::AbstractArray{<:AbstractFloat, 1};
        k::Int = 3
    )
"""
function tournament_selection(
    fit::AbstractArray{<:AbstractFloat, 1};
    k::Int = 3
    )

    idx = sample(1:length(fit), k, replace = false)
    return argmax(idx)
end