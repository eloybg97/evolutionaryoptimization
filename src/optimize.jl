"""
    optimize(
        alg::Algorithm,
        fobjetive::Function,
        pop::AbstractArray{<:Real, 2},
        maximize::Bool,
        n_iteration::Int = 1,
        callback::Function = nothing_callback
    )
"""
function optimize(
    alg::Algorithm,
    fobjetive::Function,
    pop::AbstractArray{<:Real, 2},
    maximize::Bool,
    n_iteration::Int = 1,
    callback::Function = nothing_callback
    )

    @assert n_iteration > 0

    if maximize
        conversion_factor = 1
    else
        conversion_factor = -1
    end

    f(x) = conversion_factor * fobjetive(x)


    fit = map(f, eachrow(pop))
    best_idx = argmax(fit)

    callback(0, pop, conversion_factor * fit, best_idx)

    for i = 1:n_iteration
        pop, fit = one_step(alg, pop, fit, f)


        best_idx = argmax(fit)
        callback(i, pop, conversion_factor * fit, best_idx)
    end

    fit = conversion_factor * fit

    return pop[best_idx, :], fit[best_idx], mean(fit), std(fit)
    
end