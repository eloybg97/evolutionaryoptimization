function rosenbrock(x::AbstractArray{<:Real, 1})
    a = 1
    b = 100

    result = 0
    N = length(x) - 1
    
    for i = 1:N
        result +=  (a - x[i]) + b * (x[i + 1] - x[i]^2)^2
    end

    return result
end