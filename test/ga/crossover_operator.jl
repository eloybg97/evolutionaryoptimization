function check_actual_k(p1, p2, h1, h2)
    actualkh1 = 0
    actualkh2 = 0

    N = length(p1)
    for i=1:N
        if actualkh1 % 2 == 0 && h1[1,i] == p2[i]
            actualkh1 = actualkh1 + 1

        elseif actualkh1 % 2 == 1 && h1[1,i] == p1[i]
            actualkh1 = actualkh1 + 1
        end
    end

    for i=1:N
        if actualkh2 % 2 == 0 && h2[1,i] == p1[i]
            actualkh2 = actualkh2 + 1

        elseif actualkh2 % 2 == 1 && h2[1,i] == p2[i]
            actualkh2 = actualkh2 + 1
        end
    end

    return actualkh1, actualkh2
end

@testset "K-Point Crossover" begin
    begin
        N = 6
        expectedk = 2

        p1 = rand(N)
        p2 = rand(N)

        p1copy = copy(p1)
        p2copy = copy(p2)

        h1, h2 = k_point_crossover(p1, p2, k = expectedk)

        actualkh1, actualkh2 = check_actual_k(p1, p2, h1, h2)
              

        @test actualkh1 == expectedk
        @test actualkh2 == expectedk
        @test p1 == p1copy
        @test p2 == p2copy
        @test p1 != h1
        @test p1 != h2
        @test p2 != h1
        @test p2 != h2
        @test size(h1) == (1, N)
        @test size(h2) == (1, N)
        @test ndims(h1) == 2
        @test ndims(h2) == 2
    end

    begin
        N = 20
        expectedk = 10

        p1 = rand(N)
        p2 = rand(N)

        p1copy = copy(p1)
        p2copy = copy(p2)

        h1, h2 = k_point_crossover(p1, p2, k = expectedk)

        actualkh1, actualkh2 = check_actual_k(p1, p2, h1, h2)
              

        @test actualkh1 == expectedk
        @test actualkh2 == expectedk
        @test p1 == p1copy
        @test p2 == p2copy
        @test p1 != h1
        @test p1 != h2
        @test p2 != h1
        @test p2 != h2
        @test size(h1) == (1, N)
        @test size(h2) == (1, N)
        @test ndims(h1) == 2
        @test ndims(h2) == 2
    end

    begin
        N = 2
        expectedk = 1

        p1 = rand(N)
        p2 = rand(N)

        p1copy = copy(p1)
        p2copy = copy(p2)

        h1, h2 = k_point_crossover(p1, p2, k = expectedk)

        actualkh1, actualkh2 = check_actual_k(p1, p2, h1, h2)
              

        @test actualkh1 == expectedk
        @test actualkh2 == expectedk
        @test p1 == p1copy
        @test p2 == p2copy
        @test p1 != h1
        @test p1 != h2
        @test p2 != h1
        @test p2 != h2
        @test size(h1) == (1, N)
        @test size(h2) == (1, N)
        @test ndims(h1) == 2
        @test ndims(h2) == 2
    end

end