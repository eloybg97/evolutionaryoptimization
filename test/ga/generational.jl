@testset "Generational Genetic Algorithm" begin
    f(x) = rosenbrock(x)
    crossover(parent1, parent2) = k_point_crossover(parent1, parent2; k = 1) 
    selection(fit) = tournament_selection(fit; k = 3)
    mutation(child) = swap_mutation(child)

    begin
        dim = (60, 5)
        pop = rand(dim[1], dim[2])
        fit = map(f, eachrow(pop))
    
        gga = GGA(crossover, selection, mutation, 0.7, 0.1)
        
        newpop, newfit = EvolutionaryOptimization.one_step(gga, pop, fit, f)

        @test size(pop) == size(newpop) 
        @test size(fit) == size(newfit)
        @test ndims(pop) == ndims(newpop)
        @test ndims(fit) == ndims(newfit)
        @test pop != newpop
        @test maximum(fit) <= maximum(newfit)
    end
end