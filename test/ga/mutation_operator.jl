@testset "Flip Mutation" begin
    begin
        N = 10
        h = rand(N)
        hcopy = copy(h)

        m = swap_mutation(h)

        @test h == hcopy
        @test size(m) == (1, N)
        @test ndims(m) == 2
    end

end