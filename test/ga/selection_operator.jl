@testset "Tournament Selection" begin
    begin
        fit = rand(20)
        copyfit = copy(fit)

        p = tournament_selection(fit)

        @test fit == copyfit
        @test p != argmin(fit)
    end
end