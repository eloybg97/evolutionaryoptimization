@testset "GGA Optimization" begin
    dim = (60, 5)
    f(x) = rosenbrock(x)

    crossover(parent1, parent2) = k_point_crossover(parent1, parent2; k = 1) 
    selection(fit) = tournament_selection(fit; k = 3)
    mutation(child) = swap_mutation(child)

    gga = GGA(crossover, selection, mutation, 0.7, 0.1)

    begin
        res = optimize(gga, f, rand(dim[1], dim[2]) , true, 5)

        @test ndims(res[1]) == 1
        @test size(res[1])  == (dim[2],)
        @test f(res[1]) == res[2]
        @test res[2] >= res[3]
    end

    begin
        res = optimize(gga, f, rand(dim[1], dim[2]) , false, 5)

        @test ndims(res[1]) == 1
        @test size(res[1])  == (dim[2],)
        @test f(res[1]) == res[2]
        @test res[2] <= res[3]
    end
end