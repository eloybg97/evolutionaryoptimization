using Test
using EvolutionaryOptimization

import Statistics: mean
include("function.jl")


@time include("optimize.jl")

@time include("ga/selection_operator.jl")
@time include("ga/crossover_operator.jl")
@time include("ga/mutation_operator.jl")
@time include("ga/generational.jl")